import { Position, Velocity, Constants, Colors,  Utils } from "./utils.js";

export default class Ball {
  constructor(color, position, velocity, size) {
    this.color = color
    this.position = position
    this.velocity = velocity
    this.size = size
    if (!color && !position && !velocity && !size) {
      this.regen()
    }
  }

  draw(ctx) {
    ctx.beginPath();
    ctx.arc(this.position.x, this.position.y, Constants.scale * this.size, 0, 2 * Math.PI)
    ctx.fillStyle = this.color
    ctx.fill()
  }

  isEaten(player) {
    if (Utils.getDistance(player.position, this.position) < Math.max(player.size, this.size) * Constants.scale) {
      return true
    }
    return false
  }

  regen() {
    this.position = new Position(Math.random() * Constants.canvasWidth, Math.random() * Constants.canvasHeight)

    this.velocity = new Velocity(Math.random() * 3 * Math.random() < 0.5 ? -1 : 1, Math.random() * 3 * Math.random() < 0.5 ? -1 : 1)

    this.size = Math.floor(Math.random() * 6) + 1

    this.color = Colors.random()
  }

  update(player) {
    if (this.isEaten(player)) {
      if (player.size < this.size) {
        player.gameOver()
      } else {
        // if (player.size > 24) {
        //   player.gameOver()
        // }
        this.regen()
        player.size += 0.3
      }
    }
    this.checkBounds()
    this.position.x += this.velocity.x
    this.position.y += this.velocity.y

    return this
  }

  checkBounds() {
    if (this.position.x < -this.size*Constants.scale) {
      this.position.x = Constants.canvasWidth + this.size*Constants.scale
    } else if (this.position.x > Constants.canvasWidth + this.size*Constants.scale) {
      this.position.x = -this.size*Constants.scale
    }
    if (this.position.y < -this.size*Constants.scale) {
      this.position.y = Constants.canvasHeight + this.size*Constants.scale
    } else if (this.position.y > Constants.canvasHeight + this.size*Constants.scale) {
      this.position.y = -this.size*Constants.scale
    }
  }
}