export class Colors{
  static colorsList = [
    '#85B464',
    '#E2D269',
    '#EA915E',
    '#BC677B',
    '#7A4E8A'
  ]

  constructor() { }
  
  static random() {
    return this.colorsList[Math.floor(Math.random() * this.colorsList.length)]
  }
}

export class Position {
  constructor(x = 0, y = 0) {
    this.x = x
    this.y = y
  }
}

export class Velocity {
  constructor(x = 1, y = 1) {
    this.x = x
    this.y = y
  }
}

export const Constants = {
  canvasWidth: 100,
  canvasHeight: 100,
  scale: 10
}

export class Utils {
  static getDistance(pos1, pos2) {
    return Math.sqrt(Math.pow(pos2.x - pos1.x,2) + Math.pow(pos2.y - pos1.y,2))
  }
}