import { Position, Constants } from "./utils.js";

export default class Player {
  constructor(color = '#5391AE', position = new Position(Constants.canvasWidth / 2, Constants.canvasHeight / 2), size = 1) {
    this.color = color
    this.position = position
    this.size = size
    this.playing = true
  }

  draw(ctx) {
    ctx.beginPath();
    ctx.arc(this.position.x, this.position.y, Constants.scale * this.size, 0, 2 * Math.PI)
    ctx.fillStyle = this.color
    ctx.fill()
  }

  gameOver() {
    this.playing = false
    this.size = 1
    this.position = new Position(Constants.canvasWidth / 2, Constants.canvasHeight / 2)
  }
}