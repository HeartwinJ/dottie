import { Position, Constants } from './utils.js'
import Player from './player.js'
import Ball from './ball.js'


const canvas = document.createElement('canvas')
canvas.setAttribute('tabindex', '1')
/** @type {CanvasRenderingContext2D} */
const ctx = canvas.getContext('2d')

let paused = true

const player = new Player()
const ballsList = []

canvas.height = document.body.scrollHeight
canvas.width = document.body.scrollWidth
document.body.appendChild(canvas)

Constants.canvasHeight = canvas.height
Constants.canvasWidth = canvas.width

window.addEventListener('resize', onResize)
canvas.addEventListener('mousemove', setMousePosition)
canvas.addEventListener('keypress', handleKeyPress)
canvas.addEventListener('click', togglePause)

function onResize(evt) {
  canvas.height = evt.target.innerHeight
  canvas.width = evt.target.innerWidth

  Constants.canvasHeight = canvas.height
  Constants.canvasWidth = canvas.width
}

function setMousePosition(evt) {
  player.position = new Position(evt.clientX, evt.clientY)
}

function handleKeyPress(evt) {
  switch (evt.key) {
    case 'p':
      togglePause()
      break
    case 'r':
      player.gameOver()
      break
  }
}

function togglePause(evt) {
  paused = !paused
}

function drawPauseMenu() {
  ctx.fillStyle = '#232531'
  ctx.fillRect(0, 0, Constants.canvasWidth, Constants.canvasHeight)
  ctx.strokeStyle = '#fff'
  ctx.fillStyle = '#fff'
  ctx.textAlign = 'center'
  ctx.font = '96px Arial'
  ctx.strokeText('PAUSED', Constants.canvasWidth / 2, Constants.canvasHeight / 2)
  ctx.font = '24px Arial'
  ctx.fillText('Click Anywhere to Resume', Constants.canvasWidth / 2, (Constants.canvasHeight / 2) + 70)
  ctx.font = '18px Arial'
  ctx.fillText('Press \'R\' to Restart', Constants.canvasWidth / 2, (Constants.canvasHeight / 2) + 100)
}

function init() {
  ctx.fillStyle = '#f2f3f4'
  ctx.fillRect(0, 0, Constants.canvasWidth, Constants.canvasHeight)

  genBalls()
}

function genBalls() {
  for (let index = 0; index < 40; index++) {
    ballsList.push(new Ball())
  }
}

function clearBalls() {
  ballsList.length = 0
}

function gameLoop() {
  if (!paused) {
    if (!player.playing) {
      togglePause()
      clearBalls()
      genBalls()
      player.playing = true
    }
    ctx.clearRect(0, 0, Constants.canvasWidth, Constants.canvasHeight)
    ctx.fillStyle = '#f2f3f4'
    ctx.fillRect(0, 0, Constants.canvasWidth, Constants.canvasHeight)
    ballsList.forEach(ball => {
      ball.update(player).draw(ctx)
    });
    player.draw(ctx)
  } else {
    drawPauseMenu(ctx)
  }
  requestAnimationFrame(gameLoop)
}

init()
requestAnimationFrame(gameLoop)